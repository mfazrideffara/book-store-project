package com.example.bookstore.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@Entity
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt, id"}, 
        allowGetters = true)

public class Book implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String titleBook;

    @NotBlank
    private int author;
    
    private int publisher;
    
    private int genre;
    
    private int price;
    
    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date releaseDate;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

	public Long getId() {
		return id;
	}

	public String getTitleBook() {
		return titleBook;
	}

	public int getAuthor() {
		return author;
	}

	public int getPublisher() {
		return publisher;
	}

	public int getGenre() {
		return genre;
	}

	public int getPrice() {
		return price;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTitleBook(String titleBook) {
		this.titleBook = titleBook;
	}

	public void setAuthor(int author) {
		this.author = author;
	}

	public void setPublisher(int publisher) {
		this.publisher = publisher;
	}

	public void setGenre(int genre) {
		this.genre = genre;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}