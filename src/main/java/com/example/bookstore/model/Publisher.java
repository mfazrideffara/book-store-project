package com.example.bookstore.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

	@Entity
	@EntityListeners(AuditingEntityListener.class)
	@JsonIgnoreProperties(value = {"createdAt", "updatedAt", "id"}, 
	        allowGetters = true)
	
	public class Publisher implements Serializable {
	    /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

	    @NotBlank
	    private String publisherName;
	    
	    private String country;
	    
	    private String quality;

	    private double productionCost;

	    @Column(nullable = false, updatable = false)
	    @Temporal(TemporalType.TIMESTAMP)
	    @CreatedDate
	    private Date createdAt;

	    @Column(nullable = false)
	    @Temporal(TemporalType.TIMESTAMP)
	    @LastModifiedDate
	    private Date updatedAt;

		public Long getId() {
			return id;
		}

		public String getPublisherName() {
			return publisherName;
		}

		public String getCountry() {
			return country;
		}

		public String getQuality() {
			return quality;
		}

		public double getProductionCost() {
			return productionCost;
		}

		public Date getCreatedAt() {
			return createdAt;
		}

		public Date getUpdatedAt() {
			return updatedAt;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public void setPublisherName(String publisherName) {
			this.publisherName = publisherName;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public void setQuality(String quality) {
			this.quality = quality;
		}

		public void setProductionCost(double productionCost) {
			this.productionCost = productionCost;
		}

		public void setCreatedAt(Date createdAt) {
			this.createdAt = createdAt;
		}

		public void setUpdatedAt(Date updatedAt) {
			this.updatedAt = updatedAt;
		}
}