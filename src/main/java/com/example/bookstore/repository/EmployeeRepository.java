package com.example.bookstore.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.example.bookstore.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

	List<Employee> findByName(String name);

	List<Employee> findByJobDescription(String jobDescription);
	
	@Query(value = "SELECT * FROM Employee WHERE age > (SELECT AVG(age) FROM Employee)", nativeQuery=true)
	List<Employee> findEmployeeAboveAvgAge();
	
	@Query(value = "SELECT AVG(Age) FROM Employee", nativeQuery=true)
	Double getAvgAgeEmployee();
	
	@Query(value = "SELECT * FROM Employee", nativeQuery=true)
	List<Employee> findAllEmployee();
	
	@Modifying
	@Query
	(value = "INSERT INTO Employee( address, age, job_Description, name, salary) VALUES (:address, :age, :jobDescription, :name, :salary )", nativeQuery=true)
	@Transactional
	void saveEmployee(@Param("address") String address, @Param("age") Integer age,
			   @Param("jobDescription") String jobDescription, @Param("name") String name, 
			   @Param("salary") Double salary);
	
	@Modifying
	@Query
	(value = "DELETE FROM Employee Where employee_id = :id", nativeQuery = true)
	@Transactional
	void deleteEmployee(@Param("id") Long id);
	
	@Modifying
	@Query
	(value = "UPDATE Employee SET address = :address, age = :age, job_Description = :jobDescription, name = :name, salary = :salary WHERE employee_id = :id", nativeQuery = true)
	@Transactional
	void updateEmployee(@Param("id") Long id, @Param("address") String address, @Param("age") Integer age,
			   @Param("jobDescription") String jobDescription, @Param("name") String name, 
			   @Param("salary") Double salary);
		
}
