package com.example.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.bookstore.model.Publisher;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long>{

	List<Publisher> findByPublisherName(String publisherName);

}