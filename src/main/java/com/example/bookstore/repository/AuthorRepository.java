package com.example.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.bookstore.model.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long>{
	
	List<Author> findByFirstName(String firstName);

	@Query(value = "SELECT * FROM Author", nativeQuery=true)
	List<Author> findAllAuthor();
	
	@Modifying
	@Query
	(value = "INSERT INTO Author( age, country, first_Name, last_Name ) VALUES (:age, :country, :firstName, :lastName)", nativeQuery=true)
	@Transactional
	void saveAuthor(@Param("age") Integer age, @Param("country") String country, 
					@Param("firstName") String firstName, @Param("lastName") String lastName
			       );
	
	@Modifying
	@Query
	(value = "DELETE FROM Author Where id = :id", nativeQuery = true)
	@Transactional
	void deleteAuthor(@Param("id") Long id);
	
	@Modifying
	@Query
	(value = "UPDATE Author SET age = :age, country = :country, first_Name = :firstName, last_Name = :lastName WHERE id = :id", nativeQuery = true)
	@Transactional
	void updateAuthor(@Param("id") Long id, @Param("age") Integer age, @Param("country") String country, 
				      @Param("firstName") String firstName, @Param("lastName") String lastName
			 		 );
}
