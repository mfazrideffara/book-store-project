package com.example.bookstore.controller;

import com.example.bookstore.repository.PublisherRepository;
import com.example.bookstore.exception.ResourceNotFoundException;
import com.example.bookstore.model.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PublisherController {

    @Autowired
    PublisherRepository publisherRepository;

 // Get All Publisher
    @GetMapping("/publisher")
    public List<Publisher> getAllPublisher() {
        return publisherRepository.findAll();
    }
    
    // Create a new Publisher
    @PostMapping("/publisher")
    public Publisher createPublisher(@Valid @RequestBody Publisher publisher) {
        return publisherRepository.save(publisher);
    }
    
    // Get a Single Publisher
    @GetMapping("/publisher/{id}")
    public Publisher getPublisherById(@PathVariable(value = "id") Long id) {
        return publisherRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Author", "id", id));
    }
    
    // Update a Publisher
    @PutMapping("/publisher/{id}")
    public Publisher updatePublisher(@PathVariable(value = "id") Long id,
                                            @Valid @RequestBody Publisher publisherDetails) {

    	Publisher publisher = publisherRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Author", "id", id));

    	publisher.setPublisherName(publisherDetails.getPublisherName());
    	publisher.setQuality(publisherDetails.getQuality());
    	publisher.setCountry(publisherDetails.getCountry());
    	publisher.setProductionCost(publisherDetails.getProductionCost());

    	Publisher updatePublisher = publisherRepository.save(publisher);
        return updatePublisher;
    }
    
    // Delete a Publisher
    @DeleteMapping("/publisher/{id}")
    public ResponseEntity<?> deletePublisher(@PathVariable(value = "id") Long id) {
    	Publisher publisher = publisherRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Author", "id", id));

    	publisherRepository.delete(publisher);

        return ResponseEntity.ok().build();
    }
    
    // Sort Publisher by publisherName
    @GetMapping("/publisher/sort")
	public List<Publisher> sortAuthor(@RequestParam(value="publisherName")String publisherName){
		return publisherRepository.findByPublisherName(publisherName);
	}
}

