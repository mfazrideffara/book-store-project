package com.example.bookstore.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.example.bookstore.exception.ResourceNotFoundException;
import com.example.bookstore.model.Employee;
import com.example.bookstore.repository.EmployeeRepository;

@RestController
@RequestMapping("/api")
public class EmployeeController {

    @Autowired
    EmployeeRepository employeeRepository;

 // Get All Employee
    @GetMapping("/employee")
    public HashMap<String, Object> getAllEmployee() {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	List<Employee> listEmployees = employeeRepository.findAll();
    	String message;
    	if(listEmployees.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Total", listEmployees.size());
    	showHashMap.put("Data", listEmployees);
    	
    	return showHashMap;
    }
    
    // Create a new Employee
    @PostMapping("/employee")
    public Employee createEmployee(@Valid @RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }
    
    // Get a Single Employee
    @GetMapping("/employee/{id}")
    public Employee getEmployeeById(@PathVariable(value = "id") Long id) {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", id));
    }
    
    // Update a Employee
    @PutMapping("/employee/{id}")
    public Employee updateEmployee(@PathVariable(value = "id") Long id,
                                            @Valid @RequestBody Employee employeeDetails) {

    	Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", id));

    	employee.setName(employeeDetails.getName());
    	employee.setAddress(employeeDetails.getAddress());
    	employee.setAge(employeeDetails.getAge());
    	employee.setJobDescription(employeeDetails.getJobDescription());
    	employee.setSalary(employeeDetails.getSalary());
    	
        Employee updateEmployee = employeeRepository.save(employee);
        return updateEmployee;
    }
    
    // Delete a Employee
    @DeleteMapping("/employee/{id}")
    public ResponseEntity<?> deleteEmployee(@PathVariable(value = "id") Long id) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", id));

        employeeRepository.delete(employee);

        return ResponseEntity.ok().build();
    }
    
    //Get Employee with JobDesc
    @GetMapping("/employee/readByJobDesc")
    public HashMap<String, Object> getEmployeeByJobDescription(@RequestParam(value = "jobDescription") String jobDescription) {
    	// Membuat object hashmap yang nantinya semua keterangan yang sesuai akan di simpan disini 
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	
    	//Membuat object arraylist yang nantinya list employee akan di simpan di sini
    	List<Employee> listEmployees = employeeRepository.findByJobDescription(jobDescription);
    	
    	// Membuat sebuah variable untuk menyimpan message
    	String message;
    	
    	//Sebuah Decision untuk memunculkan message yang sesuai
    	if(listEmployees.isEmpty()) {
    		message = "Read All "+jobDescription+" Failed!";
    	} else {
    		message = "Read All "+jobDescription+" Success!";
    	}
    	
    	//Menginisialisasi setiap key dan value untuk result atau response
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Data", listEmployees.size());
    	showHashMap.put("Data", listEmployees);
    	
    	return showHashMap;
    }
    
    // Get employees whose age is above average
    @GetMapping("/employee/readByAge/aboveAvg")
    public HashMap<String, Object> getEmployeeAvgAge() {
    	// Membuat object hashmap yang nantinya semua keterangan yang sesuai akan di simpan disini 
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	
    	//Membuat object arraylist yang nantinya list employee akan di simpan di sini
    	List<Employee> listEmployees = employeeRepository.findEmployeeAboveAvgAge();
    	
    	// Membuat sebuah variable untuk menyimpan message
    	String message;
    	
    	//Sebuah Decision untuk memunculkan message yang sesuai
    	if(listEmployees.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	
    	//Menginisialisasi setiap key dan value untuk result atau response
    	showHashMap.put("Message", message);
    	showHashMap.put("Rata-rata Umur", employeeRepository.getAvgAgeEmployee());
    	showHashMap.put("Data", listEmployees);
    	
    	return showHashMap;
    }
    
    // Create a new Employee
    @PostMapping("/employee/addAll")
    public HashMap<String, Object> createEmployee(@Valid @RequestBody Employee... employee) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	
    	@Valid Employee[] listEmployees = employee;
    	
    	String message;
    	
    	for(Employee emp : listEmployees) {
    		 employeeRepository.save(emp);
    	}
    
    	if(listEmployees == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listEmployees.length);
    	showHashMap.put("Data", listEmployees);
    	
    	return showHashMap;
    } 
    
    //get Max Salary From Employee
    public double getMaxSalary() {
    	List<Employee> listEmployees = employeeRepository.findAll();
    	double maxSalary = listEmployees.get(0).getSalary();
	    for(int i = 1; i < listEmployees.size(); i++){
		    if(listEmployees.get(i).getSalary() > maxSalary){
		    	maxSalary = listEmployees.get(i).getSalary();
			}
	    }

    	return maxSalary;
    }
    
    // Get Employee Whose have
    @GetMapping("/employee/salary/max")
    public HashMap<String, Object> getEmployeeWithMaxSalary() {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	List<Employee> listEmployees = employeeRepository.findAll();
    	List<Employee> resultList = new ArrayList<Employee>();
    	String message;
    	
    	if(listEmployees.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	
    	for(Employee emp : listEmployees) {
    		if(emp.getSalary() == getMaxSalary()) {
    			resultList.add(emp);
    		}
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Salary Tertinggi", getMaxSalary());
    	showHashMap.put("Total Data", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    
    // Get Employee by name
    @GetMapping("/employee/readByName")
	public HashMap<String, Object> getEmployeeByName(@RequestParam(value="name")String name){
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	List<Employee> listEmployees = employeeRepository.findAll();
    	List<Employee> resultList = new ArrayList<Employee>();
    	String message;
    	
    	if(listEmployees.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	
    	for(Employee emp : listEmployees) {
    		if(emp.getName().contains(name)) {
    			resultList.add(emp);
    		}
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Data", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
	}
}
