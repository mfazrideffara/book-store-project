package com.example.bookstore.controller;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstore.exception.ResourceNotFoundException;
import com.example.bookstore.model.Author;
import com.example.bookstore.repository.AuthorRepository;

@RestController
@RequestMapping("/api")
public class AuthorController {

    @Autowired
    AuthorRepository authorRepository;

    //Get All Author
    @GetMapping("/author")
    public HashMap<String, Object> getAllAuthor() {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	List<Author> listAuthors = authorRepository.findAllAuthor();
    	String message;
    	if(listAuthors.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Total", listAuthors.size());
    	showHashMap.put("Data", listAuthors);
    	
    	return showHashMap;
    }
    
    // Create a new Author
    @PostMapping("/author")
    public HashMap<String, Object> createAuthor(@Valid @RequestBody Author... author) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid Author[] listAuthors = author;
    	String message;
    	
    	for(Author a : listAuthors) {
    		authorRepository.saveAuthor(a.getAge(),
    									a.getCountry(),
    									a.getFirstName(),
    									a.getLastName());
    	}
    
    	if(listAuthors == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listAuthors.length);
    	showHashMap.put("Data", listAuthors);
    	
    	return showHashMap;
    }
    
    // Get a Single Author
    @GetMapping("/author/{id}")
    public Author getAuthorById(@PathVariable(value = "id") Long id) {
        return authorRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Author", "id", id));
    }
    
    // Update a Author
    @PutMapping("/author/{id}")
    public HashMap<String, Object> updateAuthor(@PathVariable(value = "id") Long id,
            @Valid @RequestBody Author... authorDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid Author[] listAuthors = authorDetails;
    	String message;
    	
    	for(Author a : listAuthors) {
    		authorRepository.updateAuthor(id,
    										a.getAge(),
											a.getCountry(),
											a.getFirstName(),
											a.getLastName());
    	}
    	if(listAuthors == null) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", listAuthors.length);
    	showHashMap.put("Data", listAuthors);
    	
    	return showHashMap;
    }
    
    // Delete a Author
    @DeleteMapping("/author/{id}")
    public void deleteAuthor(@PathVariable(value = "id") Long id) {
    	
        authorRepository.deleteAuthor(id);
    }
}
