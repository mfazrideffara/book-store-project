package com.example.bookstore.controller;

import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.bookstore.exception.ResourceNotFoundException;
import com.example.bookstore.model.Employee;
import com.example.bookstore.repository.EmployeeRepository;

@RestController
@RequestMapping("/apis")
public class EmployeeControllerQuery {

    @Autowired
    EmployeeRepository employeeRepository;

 // Get All Employee
    @GetMapping("/employee")
    public HashMap<String, Object> getAllEmployee() {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	List<Employee> listEmployees = employeeRepository.findAllEmployee();
    	String message;
    	if(listEmployees.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Total", listEmployees.size());
    	showHashMap.put("Data", listEmployees);
    	
    	return showHashMap;
    }
    
    // Create a new Employee
    @PostMapping("/employee")
    public HashMap<String, Object> createEmployee(@Valid @RequestBody Employee... employee) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid Employee[] listEmployees = employee;
    	String message;
    	
    	for(Employee emp : listEmployees) {
    		employeeRepository.saveEmployee(emp.getAddress(), emp.getAge(), emp.getJobDescription(), emp.getName(), emp.getSalary());
    	}
    
    	if(listEmployees == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listEmployees.length);
    	showHashMap.put("Data", listEmployees);
    	
    	return showHashMap;
    }
    
    // Get a Single Employee
    @GetMapping("/employee/{id}")
    public Employee getEmployeeById(@PathVariable(value = "id") Long id) {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", id));
    }
    
    // Update a Employee
    @PutMapping("/employee/{id}")
    public HashMap<String, Object> updateEmployee(@PathVariable(value = "id") Long id,
            @Valid @RequestBody Employee... employeeDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid Employee[] listEmployees = employeeDetails;
    	String message;
    	
    	for(Employee emp : listEmployees) {
    		employeeRepository.updateEmployee(id,emp.getAddress(), emp.getAge(), emp.getJobDescription(), emp.getName(), emp.getSalary());
    	}
    	if(listEmployees == null) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", listEmployees.length);
    	showHashMap.put("Data", listEmployees);
    	
    	return showHashMap;
    }
    
    // Delete a Employee
    @DeleteMapping("/employee/{id}")
    public void deleteEmployee(@PathVariable(value = "id") Long id) {
    	
        employeeRepository.deleteEmployee(id);
    }
}
