package com.example.bookstore.controller;

import com.example.bookstore.repository.BookRepository;
import com.example.bookstore.exception.ResourceNotFoundException;
import com.example.bookstore.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class BookController {

    @Autowired
    BookRepository bookRepository;

    // Get All Book
    @GetMapping("/book")
    public List<Book> getAllBook() {
        return bookRepository.findAll();
    }
    
    // Create a new Book
    @PostMapping("/book")
    public Book createBook(@Valid @RequestBody Book book) {
        return bookRepository.save(book);
    }
    
    // Get a Single Book
    @GetMapping("/book/{id}")
    public Book getBookById(@PathVariable(value = "id") Long id) {
        return bookRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Book", "id", id));
    }
    
    // Update a Book
    @PutMapping("/book/{id}")
    public Book updateBook(@PathVariable(value = "id") Long id,
                                            @Valid @RequestBody Book bookDetails) {

    	Book book = bookRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Book", "id", id));

    	book.setTitleBook(bookDetails.getTitleBook());
    	book.setAuthor(bookDetails.getAuthor());
        book.setGenre(bookDetails.getGenre());
        book.setPublisher(bookDetails.getPublisher());
        book.setPrice(bookDetails.getPrice());
        book.setReleaseDate(bookDetails.getReleaseDate());

        Book updateBook = bookRepository.save(book);
        return updateBook;
    }
    
    // Delete a Book
    @DeleteMapping("/book/{id}")
    public ResponseEntity<?> deleteBook(@PathVariable(value = "id") Long id) {
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Book", "id", id));

        bookRepository.delete(book);

        return ResponseEntity.ok().build();
    }
    
    // Sort Book by titleBook
    @GetMapping("/book/sort")
	public List<Book> sortBook(@RequestParam(value="titleBook")String titleBook){
		return bookRepository.findByTitleBook(titleBook);
	}
}